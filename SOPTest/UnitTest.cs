﻿/*
 * David JT; December 18, 2020.
 */

using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SOP;

namespace SOPTest
{
    [TestClass]
    public class UnitTest
    {
        private IntData _unsortedInts = new IntData(new int[] { 20, -10, 15, 5, 20, 25, 35, 8, 9 });
        private IntData _sortedInts = new IntData(new int[] { -10, 5, 8, 9, 15, 20, 20, 25, 35 });
        private StringData _unsortedStrings = new StringData(new string[] { "abc", "cba", "bcd", "a" });
        private StringData _sortedStrings = new StringData(new string[] { "a", "abc", "bcd", "cba" });

        [TestMethod]
        public void TestInsertionSort()
        {
            TestSortingAlgorithm(Algorithms.InsertionSort);
        }

        [TestMethod]
        public void TestMergeSort()
        {
            TestSortingAlgorithm(Algorithms.MergeSort);
        }

        [TestMethod]
        public void TestHeapSort()
        {
            TestSortingAlgorithm(Algorithms.HeapSort);
        }

        [TestMethod]
        public void TestQuickSort()
        {
            TestSortingAlgorithm(Algorithms.QuickSort);
        }

        [TestMethod]
        public void TestIntroSort()
        {
            TestSortingAlgorithm(Algorithms.IntroSort);
        }

        [TestMethod]
        public void TestNetSort()
        {
            TestSortingAlgorithm(Algorithms.NetSort);
        }

        private void TestSortingAlgorithm(SortingAlgorithm algorithm)
        {
            // Test with unsorted integers.
            IntData resultUnsortedInts = new IntData(_unsortedInts);
            algorithm.Sort(resultUnsortedInts);
            CollectionAssert.AreEqual(resultUnsortedInts.GetAll(), _sortedInts.GetAll());

            // Test with sorted integers.
            IntData resultSortedInts = new IntData(_sortedInts);
            algorithm.Sort(resultSortedInts);
            CollectionAssert.AreEqual(resultSortedInts.GetAll(), _sortedInts.GetAll());

            // Test with unsorted strings.
            StringData resultUnsortedStrings = new StringData(_unsortedStrings);
            algorithm.Sort(resultUnsortedStrings);
            CollectionAssert.AreEqual(resultUnsortedStrings.GetAll(), _sortedStrings.GetAll());
        }
    }
}
