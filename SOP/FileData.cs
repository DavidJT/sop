﻿/*
 * David JT; December 18, 2020.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOP
{
    public class FileData : StringData
    {
        public FileData(string[] data) : base(data) { }

        public FileData(FileData data) : base(data) { }

        /// <summary>
        /// Creates a new FileData object with all lines in a given file read as strings.
        /// </summary>
        /// <param name="filepath">The path to a file to read all lines from.</param>
        public FileData(string filepath)
        {
            _data = File.ReadAllLines(filepath);
        }

        public override Data<string> Copy()
        {
            return new FileData(this);
        }

        /// <summary>
        /// Writes all the strings in this FileData object on separate lines in a given file.
        /// </summary>
        /// <param name="filepath">The path to a file to write to.</param>
        public void Write(string filepath)
        {
            File.WriteAllLines(filepath, _data);
        }
    }
}
