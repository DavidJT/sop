﻿/*
 * David JT; December 18, 2020.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOP
{
    public class HeapSort : SortingAlgorithm
    {
        /// <summary>
        /// Sorts the given Data object in-place using heap sort.
        /// </summary>
        /// <typeparam name="T">The type of the Data object.</typeparam>
        /// <param name="data">The Data object to sort in-place.</param>
        public override void Sort<T>(Data<T> data)
        {
            Sort(data, 0, data.Length - 1);
        }

        /// <summary>
        /// Sorts the given Data object in-place using heap sort between a given start and end
        /// index.
        /// </summary>
        /// <typeparam name="T">The type of the Data object.</typeparam>
        /// <param name="data">The Data object to sort in-place.</param>
        /// <param name="start">The inclusive index to sort from.</param>
        /// <param name="end">The inclusive index to sort to.</param>
        public void Sort<T>(Data<T> data, int start, int end) where T : IComparable<T>
        {
            BuildHeap(data, start, end);

            // Continuously select the largest element as the root of the binary heap.
            for (int i = end; i >= start + 1; i--)
            {
                (data[start], data[i]) = (data[i], data[start]);
                Heapify(data, start, start, i);
            }
        }

        /// <summary>
        /// Builds a binary heap in the given Data object.
        /// </summary>
        /// <typeparam name="T">The type of the Data object.</typeparam>
        /// <param name="data">The Data object to sort in-place.</param>
        /// <param name="start">The inclusive index to build from.</param>
        /// <param name="end">The inclusive index to build to.</param>
        private void BuildHeap<T>(Data<T> data, int start, int end) where T : IComparable<T>
        {
            int length = end - start + 1;

            // Ensure that all subtrees are binary heaps by going upwards from the second level
            // from the bottom.
            for (int i = length / 2 - 1; i >= 0; i--)
            {
                Heapify(data, start, start + i, end + 1);
            }
        }

        /// <summary>
        /// Ensures that a part of the given Data object from the given parent node's index and
        /// down to a maximum index is a binary heap.
        /// </summary>
        /// <typeparam name="T">The type of the Data object.</typeparam>
        /// <param name="data">The Data object to sort in-place.</param>
        /// <param name="start">The inclusive index we are sorting from.</param>
        /// <param name="parent">The index of the parent node to check from.</param>
        /// <param name="max">The exlusive maximum index that will be checked.</param>
        private void Heapify<T>(Data<T> data, int start, int parent, int max) where T : IComparable<T>
        {
            int left = start + 2 * (parent - start) + 1;
            int right = start + 2 * (parent - start) + 2;

            // Figure out which element is the largest.
            int largest = parent;
            if (left < max && data[left].CompareTo(data[largest]) > 0)
            {
                largest = left;
            }
            if (right < max && data[right].CompareTo(data[largest]) > 0)
            {
                largest = right;
            }

            // If this is not currently a binary heap, we need to swap the largest element with the
            // current root, and then we should recursively continue down the levels of tree.
            if (largest != parent)
            {
                (data[parent], data[largest]) = (data[largest], data[parent]);
                Heapify(data, start, largest, max);
            }
        }
    }
}
