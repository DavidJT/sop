﻿/*
 * David JT; December 18, 2020.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOP
{
    public class InsertionSort : SortingAlgorithm
    {
        /// <summary>
        /// Sorts the given Data object in-place using insertion sort.
        /// </summary>
        /// <typeparam name="T">The type of the Data object.</typeparam>
        /// <param name="data">The Data object to sort in-place.</param>
        public override void Sort<T>(Data<T> data)
        {
            Sort(data, 0, data.Length - 1);
        }

        /// <summary>
        /// Sorts the given Data object in-place using insertion sort between a given start and end
        /// index.
        /// </summary>
        /// <typeparam name="T">The type of the Data object.</typeparam>
        /// <param name="data">The Data object to sort in-place.</param>
        /// <param name="start">The inclusive index to sort from.</param>
        /// <param name="end">The inclusive index to sort to.</param>
        public void Sort<T>(Data<T> data, int start, int end) where T : IComparable<T>
        {
            // Go through the data array from left to right.
            for (int i = start + 1; i <= end; i++)
            {
                // Remember the current element because it will be overwritten.
                T value = data[i];

                // Starting from the current element, go through the array from right to left while
                // moving each element that is greater than the current element to the right.
                int j = i - 1;
                while (j >= start && data[j].CompareTo(value) > 0)
                {
                    data[j + 1] = data[j];
                    j--;
                }

                // Insert the current element into its correct position.
                data[j + 1] = value;
            }
        }
    }
}
