﻿/*
 * David JT; December 18, 2020.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOP
{
    public class RandomIntData : IntData
    {
        public RandomIntData(int[] data) : base(data) { }

        public RandomIntData(RandomIntData data) : base(data) { }

        /// <summary>
        /// Creates a new Data object of a specified amount of unsorted integers that are
        /// pseudorandomly generated within the maximum range for a 32-bit integer.
        /// </summary>
        /// <param name="amount">The amount of integers to generate.</param>
        public RandomIntData(int amount) : this(amount, Int32.MinValue, Int32.MaxValue) { }

        /// <summary>
        /// Creates a new Data object of a specified amount of unsorted integers that are
        /// pseudorandomly generated within a given range.
        /// </summary>
        /// <param name="amount">The amount of integers to generate.</param>
        /// <param name="min">The inclusive minimum value for the generated integers.</param>
        /// <param name="max">The exclusive maximum value for the generated integers.</param>
        public RandomIntData(int amount, int min, int max)
        {
            _data = new int[amount];

            Random random = new Random();
            for (int i = 0; i < amount; i++)
            {
                _data[i] = random.Next(min, max);
            }
        }

        public override Data<int> Copy()
        {
            return new RandomIntData(this);
        }
    }
}
