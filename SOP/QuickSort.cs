﻿/*
 * David JT; December 18, 2020.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOP
{
    public class QuickSort : SortingAlgorithm
    {
        /// <summary>
        /// Sorts the given Data object in-place using quicksort.
        /// </summary>
        /// <typeparam name="T">The type of the Data object.</typeparam>
        /// <param name="data">The Data object to sort in-place.</param>
        public override void Sort<T>(Data<T> data)
        {
            Sort(data, 0, data.Length - 1);
        }

        /// <summary>
        /// Sorts the given Data object in-place using quicksort between a left and right index.
        /// </summary>
        /// <typeparam name="T">The type of the Data object.</typeparam>
        /// <param name="data">The Data object to sort in-place.</param>
        /// <param name="left">The inclusive left index to sort from.</param>
        /// <param name="right">The inclusive right index to sort to.</param>
        private void Sort<T>(Data<T> data, int left, int right) where T : IComparable<T>
        {
            int pivot = Partition(data, left, right);

            // Sort subparts of the data that are at least of length 2.
            if (pivot - left >= 2)
            {
                Sort(data, left, pivot - 1);
            }
            if (right - pivot >= 2)
            {
                Sort(data, pivot + 1, right);
            }
        }

        /// <summary>
        /// Returns the pivot index after partitioning the Data so that all values on the left and
        /// on the right of the pivot are smaller and greater than the pivot value, respectively.
        /// </summary>
        /// <typeparam name="T">The type of the Data object.</typeparam>
        /// <param name="data">The Data object to sort in-place.</param>
        /// <param name="left">The inclusive left index to sort from.</param>
        /// <param name="right">The inclusive right index to sort to.</param>
        /// <returns>The pivot index after partitioning.</returns>
        public int Partition<T>(Data<T> data, int left, int right) where T : IComparable<T>
        {
            int pivot = SelectPivot(data, left, right);
            T pivotValue = data[pivot];

            // Store the pivot value on the right so we can go through all other elements.
            (data[pivot], data[right]) = (data[right], data[pivot]);

            // Move elements that are smaller than the pivot value to the left.
            int swapIndex = left;
            for (int i = left; i < right; i++)
            {
                if (data[i].CompareTo(pivotValue) < 0)
                {
                    (data[swapIndex], data[i]) = (data[i], data[swapIndex]);
                    swapIndex++;
                }
            }

            // Move the pivot value back and return its position as the pivot.
            (data[swapIndex], data[right]) = (data[right], data[swapIndex]);
            return swapIndex;
        }

        /// <summary>
        /// Returns the pivot index based on the median-of-three strategy by selecting the median
        /// out the left, middle, and right element.
        /// </summary>
        /// <typeparam name="T">The type of the Data object.</typeparam>
        /// <param name="data">The Data object to sort in-place.</param>
        /// <param name="left">The inclusive left index to sort from.</param>
        /// <param name="right">The inclusive right index to sort to.</param>
        /// <returns>The index of the selected pivot.</returns>
        private int SelectPivot<T>(Data<T> data, int left, int right) where T : IComparable<T>
        {
            int mid = (left + right) / 2;

            // Figure out which element out of the left, middle, and right element is the median.
            if (data[left].CompareTo(data[mid]) > 0 && data[left].CompareTo(data[right]) < 0
                || data[left].CompareTo(data[mid]) < 0 && data[left].CompareTo(data[right]) > 0)
            {
                return left;
            }
            else if (data[mid].CompareTo(data[left]) > 0 && data[mid].CompareTo(data[right]) < 0
                || data[mid].CompareTo(data[left]) < 0 && data[mid].CompareTo(data[right]) > 0)
            {
                return mid;
            }
            return right;
        }
    }
}
