﻿/*
 * David JT; December 18, 2020.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace SOP
{
    public partial class Form1 : Form
    {
        // The amount of unsorted and sorted Data elements that should be shown at maximum in this
        // Windows Forms.
        private const int _shownDataElements = 200;

        // The Data types to show in the Form.
        private Dictionary<string, Type> _dataTypes = new Dictionary<string, Type>
        {
            { "Tilfældige heltal", typeof(RandomIntData) },
            { "Sorterede heltal", typeof(SortedIntData) },
            { "Omvendte heltal", typeof(ReversedIntData) },
            { "Ekstern fil", typeof(FileData) },
        };

        // The SortingAlgorithms to show in the Form.
        private Dictionary<string, SortingAlgorithm> _sortingAlgorithms = new Dictionary<string, SortingAlgorithm>()
        {
            { "Insertion sort", Algorithms.InsertionSort },
            { "Merge sort", Algorithms.MergeSort },
            { "Heap sort", Algorithms.HeapSort },
            { "Quicksort", Algorithms.QuickSort },
            { "Introsort (hybrid)", Algorithms.IntroSort },
            { ".NET sort (hybrid)", Algorithms.NetSort },
        };

        // Declare dynamic variables for the data since the abstract Data class can't be
        // instantiated.
        private dynamic _unsortedData;
        private dynamic _sortedData;

        /// <summary>
        /// Creates a new Sorting Algorithm Comparison Form.
        /// </summary>
        public Form1()
        {
            InitializeComponent();

            // Bind _dataTypes to the combo box with null as the second argument since we need both
            // columns for displaying the keys and remembering the values.
            dataTypeComboBox.DataSource = new BindingSource(_dataTypes, null);
            dataTypeComboBox.DisplayMember = "Key";
            dataTypeComboBox.ValueMember = "Value";

            algorithmComboBox.DataSource = new BindingSource(_sortingAlgorithms, null);
            algorithmComboBox.DisplayMember = "Key";
            algorithmComboBox.ValueMember = "Value";

            // Without any generated data, it shouldn't be possible to sort or save the data.
            sortButton.Enabled = false;
            saveFileButton.Enabled = false;

            // Call the event handler to display the relevant components for the default data type.
            dataTypeComboBox_SelectedIndexChanged(null, null);
        }

        // All the following event handlers are called from the auto-generated Windows Forms code
        // so they won't be commented with XML documentation.
        private void dataTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Type dataType = (Type)dataTypeComboBox.SelectedValue;

            // When the selected Data type is changed, we should update which inputs are available
            // to the user depending on the type.
            if (dataType == typeof(RandomIntData) || dataType == typeof(SortedIntData)
                    || dataType == typeof(ReversedIntData))
            {
                dataSizeLabel.Visible = true;
                dataSizeTextBox.Visible = true;
                generateButton.Text = "Generér data";
                saveFileButton.Visible = false;
            }
            else if (dataType == typeof(FileData))
            {
                dataSizeLabel.Visible = false;
                dataSizeTextBox.Visible = false;
                generateButton.Text = "Åbn datafil";
                saveFileButton.Visible = true;

                // An invalid input error should no longer be displayed.
                generateResultLabel.Text = "";
            }
        }

        private void dataSizeTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Generate the Data when the user presses the enter key in the data size text box.
            if (e.KeyChar == (char)Keys.Return)
            {
                generateButton_Click(sender, null);

                // Mark this event as handled to prevent Windows from making a beep sound.
                e.Handled = true;
            }
        }

        private void generateButton_Click(object sender, EventArgs e)
        {
            if (!backgroundWorker.IsBusy)
            {
                Type dataType = (Type)dataTypeComboBox.SelectedValue;

                // Pack multiple arguments of any type into an object array by boxing them so they
                // can all be passed for the asynchronous Data generation action.
                object[] arguments = new object[3];
                arguments[0] = new Action<object, DoWorkEventArgs>(GenerateData);
                arguments[1] = dataType;

                if (dataType == typeof(RandomIntData) || dataType == typeof(SortedIntData)
                    || dataType == typeof(ReversedIntData))
                {
                    try
                    {
                        arguments[2] = GetIntInput(dataSizeTextBox, 1);
                    }
                    catch (EmptyInputException)
                    {
                        generateResultLabel.Text = "Du skal skrive et antal elementer.";
                        return;
                    }
                    catch (InvalidInputException)
                    {
                        generateResultLabel.Text = "Ugyldigt antal elementer.";
                        return;
                    }
                }
                else if (dataType == typeof(FileData))
                {
                    DialogResult result = openFileDialog.ShowDialog();
                    if (result != DialogResult.OK)
                    {
                        return;
                    }

                    arguments[2] = openFileDialog.FileName;
                }

                // The given input was correct so begin generating the Data asynchronously using
                // the BackgroundWorker.
                generateResultLabel.Text = "";
                UseWaitCursor = true;
                backgroundWorker.RunWorkerAsync(arguments);
            }
        }

        private void saveFileButton_Click(object sender, EventArgs e)
        {
            DialogResult result = saveFileDialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                // If the user was allowed to click this button, the current sorted Data should be
                // of file type.
                FileData sortedData = (FileData)_sortedData;
                sortedData.Write(saveFileDialog.FileName);
            }
        }

        private void repetitionsTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Sort the Data when the user presses the enter key in the repetitions text box.
            if (e.KeyChar == (char)Keys.Return)
            {
                sortButton_Click(sender, null);

                // Mark this event as handled to prevent Windows from making a beep sound.
                e.Handled = true;
            }
        }

        private void sortButton_Click(object sender, EventArgs e)
        {
            if (!backgroundWorker.IsBusy)
            {
                object[] arguments = new object[3];
                arguments[0] = new Action<object, DoWorkEventArgs>(SortData);
                arguments[1] = algorithmComboBox.SelectedValue;

                int repetitions;
                try
                {
                    repetitions = GetIntInput(repetitionsTextBox, 1);
                }
                catch (EmptyInputException)
                {
                    repetitions = 1;
                }
                catch (InvalidInputException)
                {
                    sortResultLabel.Text = "Ugyldigt antal gentagelser.";
                    return;
                }
                arguments[2] = repetitions;

                if (repetitions == 1)
                {
                    // We don't want to just kill the BackgroundWorker while it's sorting the Data,
                    // so it isn't possible to stop in the middle of the sorting process. However,
                    // it is possible to stop the next iteration when the Data is being sorted
                    // repeatedly.
                    sortButton.Enabled = false;

                    // Automatically, everything in the the unsorted data text is selected since
                    // it's the next element in the tab order. Instead, place the cursor at the
                    // beginning of the repetitions text box without selecting any characters.
                    repetitionsTextBox.Select();
                    repetitionsTextBox.SelectionLength = 0;
                }
                else
                {
                    sortButton.Text = "Stop sortering";
                }

                sortResultLabel.Text = "";
                UseWaitCursor = true;
                backgroundWorker.RunWorkerAsync(arguments);
            }
            else
            {
                backgroundWorker.CancelAsync();
            }
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            // The first element of the boxed arguments should be the Action to perform.
            object[] arguments = (object[])e.Argument;
            Action<object, DoWorkEventArgs> action = (Action<object, DoWorkEventArgs>)arguments[0];
            
            action(sender, e);
        }

        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            sortProgressBar.Value = e.ProgressPercentage;

            // If we have a UserState, we should be in the middle of a sorting process.
            if (e.UserState != null)
            {
                object[] userState = (object[])e.UserState;
                dynamic sortedData = userState[0];
                long[] times = (long[])userState[1];

                // The sorted Data only needs to be formatted after the first completed sort.
                if (times.Length == 1)
                {
                    sortedDataTextBox.Text = FormatData(sortedData.GetAll());
                    timesTextBox.Text = times[0].ToString();
                }

                // For large amounts of repetitions, formatting all these times will take a long
                // time, but they should still fit in the TextBox. Therefore, this program for
                // scientific use shouldn't just limit the amount of time spans that are actually
                // shown to the user, but we will just wait and update them once we are finished
                // instead of (attempting to) update the TextBox potentially many times a second.
                // However, we will still avoid formatting all time spans every time by appending
                // the newest element only, and it seems fast enough to do this with the limit so
                // we won't continue adding more time spans than that until we finish sorting.
                else if (times.Length <= _shownDataElements)
                {
                    timesTextBox.Text += ", " + times.Last().ToString();
                }
            }
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // Every pair of boxed results should be a Form component whose attribute of some kind
            // needs to be updated.
            object[] result = (object[])e.Result;
            for (int i = 0; i < result.Length; i += 2)
            {
                dynamic component = result[i];

                // For Labels and TextBoxes, we should have received a string to update as their
                // Text attribute.
                if (component.GetType() == typeof(Label) || component.GetType() == typeof(TextBox))
                {
                    object value = result[i + 1];
                    if (value.GetType() == typeof(string))
                    {
                        component.Text = (string)value;
                    }
                }

                // For Buttons, we should have received either a Text value or an Enabled state.
                else if (component.GetType() == typeof(Button))
                {
                    object value = result[i + 1];
                    if (value.GetType() == typeof(string))
                    {
                        component.Text = (string)value;
                    }
                    else if (value.GetType() == typeof(bool))
                    {
                        component.Enabled = (bool)value;
                    }
                }
            }

            // Change the mouse cursor in this form back from the wait cursor, and also explicitly
            // do it immediately because sometimes the mouse has to be moved for it to take effect.
            UseWaitCursor = false;
            Cursor.Current = Cursors.Default;
        }

        /// <summary>
        /// Generates the correct Data based on the user's input.
        /// </summary>
        /// <param name="sender">The event source.</param>
        /// <param name="e">The event data.</param>
        private void GenerateData(object sender, DoWorkEventArgs e)
        {
            object[] arguments = (object[])e.Argument;
            Type dataType = (Type)arguments[1];

            // Box the correct arguments, and generate the correct type of Data based on the
            // selected Data type.
            if (dataType == typeof(RandomIntData))
            {
                // For these integer types, we will attempt to allocate enough memory for the Data
                // even if we will run out of memory. Instead of imposing arbitrary limits, this
                // program for scientific use should allow the user to generate as many numbers as
                // they would like, and the available memory would depend on the user's computer
                // and whether they are running a 32-bit or 64-bit version of the program. Since it
                // wouldn't be safe to try to recover from an OutOfMemoryException, and since it's
                // hard to avoid allocating too much memory with something like .NET's
                // MemoryFailPoint, we will just let the application crash if the user inputs a too
                // large number (which is still below the maximum for the signed integer, otherwise
                // our input method will catch it).
                int dataSize = (int)arguments[2];
                _unsortedData = new RandomIntData(dataSize, Int32.MinValue, Int32.MaxValue);
            }
            else if (dataType == typeof(SortedIntData))
            {
                int dataSize = (int)arguments[2];
                _unsortedData = new SortedIntData(dataSize, Int32.MinValue, Int32.MaxValue);
            }
            else if (dataType == typeof(ReversedIntData))
            {
                int dataSize = (int)arguments[2];
                _unsortedData = new ReversedIntData(dataSize, Int32.MinValue, Int32.MaxValue);
            }
            else if (dataType == typeof(FileData))
            {
                string dataFile = (string)arguments[2];
                _unsortedData = new FileData(dataFile);
            }

            object[] result = new object[4];
            result[0] = unsortedDataTextBox;
            result[1] = FormatData(_unsortedData.GetAll());
            result[2] = sortButton;
            result[3] = true;
            e.Result = result;
        }

        /// <summary>
        /// Sorts the Data with the selected algorithm.
        /// </summary>
        /// <param name="sender">The event source.</param>
        /// <param name="e">The event data.</param>
        private void SortData(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            object[] arguments = (object[])e.Argument;
            SortingAlgorithm algorithm = (SortingAlgorithm)arguments[1];
            int repetitions = (int)arguments[2];

            // Reset the progress bar.
            worker.ReportProgress(0);

            // Sort the _unsortedData into _sortedData the specified amount of repeated times.
            long[] times = new long[repetitions];
            long totalTime = 0;
            for (int i = 0; i < repetitions; i++)
            {
                _sortedData = _unsortedData.Copy();
                times[i] = algorithm.TimedSort(_sortedData);
                totalTime += times[i];

                // Report progress with the sorted Data and the elapsed times so far.
                int percentProgress = 100 * (i + 1) / repetitions;
                object[] userState = new object[2];
                userState[0] = _sortedData.Copy();
                userState[1] = times.Take(i + 1).ToArray();
                worker.ReportProgress(percentProgress, userState);

                if (worker.CancellationPending)
                {
                    // Since the user most likely won't be able to stop the sorting process before
                    // at least one iteration has been completed, we might as well show them the
                    // results. Therefore, we need to remember the number of completed iterations
                    // so the average time is still calculated correctly, and we only want to show
                    // the times spent for the completed iterations.
                    repetitions = i + 1;
                    times = times.Take(repetitions).ToArray();
                    break;
                }
            }

            double averageTime = (double)totalTime / repetitions;

            object[] result = new object[12];
            result[0] = sortedDataTextBox;
            result[1] = FormatData(_sortedData.GetAll());
            result[2] = timesTextBox;
            result[3] = FormatArray(times);

            result[4] = averageTimeLabel;
            result[5] = "";
            if (repetitions > 1)
            {
                result[5] = "Gennemsnit: ";
            }
            result[5] += averageTime.ToString() + " ms";

            // It is only possible to save Data of file type.
            result[6] = saveFileButton;
            result[7] = _sortedData != null && _sortedData.GetType() == typeof(FileData);
            result[8] = sortButton;
            result[9] = true;
            result[10] = sortButton;
            result[11] = "Sortér data";
            e.Result = result;
        }

        /// <summary>
        /// Returns the integer input in a given TextBox.
        /// </summary>
        /// <param name="textBox">The TextBox to read the input from.</param>
        /// <returns>The integer input.</returns>
        private int GetIntInput(TextBox textBox)
        {
            return GetIntInput(textBox, Int32.MinValue, Int32.MaxValue);
        }

        /// <summary>
        /// Returns the integer input in a given TextBox above a given minimum value.
        /// </summary>
        /// <param name="textBox">The TextBox to read the input from.</param>
        /// <param name="min">The inclusive minimum value to allow.</param>
        /// <returns>The integer input.</returns>
        private int GetIntInput(TextBox textBox, int min)
        {
            return GetIntInput(textBox, min, Int32.MaxValue);
        }

        /// <summary>
        /// Returns the integer input in a given TextBox within a given range.
        /// </summary>
        /// <param name="textBox">The TextBox to read the input from.</param>
        /// <param name="min">The inclusive minimum value to allow.</param>
        /// <param name="max">The inclusive maximum value to allow.</param>
        /// <returns>The integer input.</returns>
        private int GetIntInput(TextBox textBox, int min, int max)
        {
            string text = textBox.Text.Trim();
            if (text == "")
            {
                throw new EmptyInputException();
            }

            // If we fail parsing the input, then it isn't a valid integer.
            int number;
            try
            {
                number = Int32.Parse(text);
            }
            catch (FormatException)
            {
                throw new InvalidInputException();
            }
            catch (OverflowException)
            {
                throw new InvalidInputException();
            }

            if (number < min || number > max)
            {
                throw new InvalidInputException();
            }

            return number;
        }

        /// <summary>
        /// Formats the given data array as a comma-delimited sequence up to an amount equal to
        /// _shownDataElements.
        /// </summary>
        /// <typeparam name="T">The type of the data array.</typeparam>
        /// <param name="data">The data array to format.</param>
        /// <returns>A comma-delimited sequence of the data array.</returns>
        private string FormatData<T>(T[] data)
        {
            return FormatArray(data, _shownDataElements);
        }

        /// <summary>
        /// Formats the given array as a comma-delimited sequence.
        /// </summary>
        /// <typeparam name="T">The type of the array.</typeparam>
        /// <param name="array">The array to format.</param>
        /// <returns>A comma-delimited sequence of the array.</returns>
        private string FormatArray<T>(T[] array)
        {
            return FormatArray(array, array.Length);
        }

        /// <summary>
        /// Formats the given array as a comma-delimited sequence up to a specified maximum amount.
        /// </summary>
        /// <typeparam name="T">The type of the array.</typeparam>
        /// <param name="array">The array to format.</param>
        /// <param name="max">The maximum amount of array elements to format.</param>
        /// <returns>A comma-delimited sequence of the array.</returns>
        private string FormatArray<T>(T[] array, int max)
        {
            string result = "";

            for (int i = 0; i < max && i < array.Length; i++)
            {
                result += array[i].ToString() + ", ";
            }

            // Replace dashes with actual minuses so numbers aren't broken up across two lines, and
            // remove the last occurence of ", ".
            result = result.Replace("-", "−");
            result = result.Substring(0, result.Length - 2);

            if (array.Length > max)
            {
                result += ", ...";
            }

            return result;
        }
    }
}
