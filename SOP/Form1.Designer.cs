﻿namespace SOP
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sortButton = new System.Windows.Forms.Button();
            this.algorithmComboBox = new System.Windows.Forms.ComboBox();
            this.dataSizeTextBox = new System.Windows.Forms.TextBox();
            this.sortResultLabel = new System.Windows.Forms.Label();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.dataTypeLabel = new System.Windows.Forms.Label();
            this.dataSizeLabel = new System.Windows.Forms.Label();
            this.algorithmLabel = new System.Windows.Forms.Label();
            this.dataTypeComboBox = new System.Windows.Forms.ComboBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.generateButton = new System.Windows.Forms.Button();
            this.unsortedDataTextBox = new System.Windows.Forms.TextBox();
            this.sortedDataTextBox = new System.Windows.Forms.TextBox();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.saveFileButton = new System.Windows.Forms.Button();
            this.repetitionsTextBox = new System.Windows.Forms.TextBox();
            this.repetitionsLabel = new System.Windows.Forms.Label();
            this.timesTextBox = new System.Windows.Forms.TextBox();
            this.sortProgressBar = new System.Windows.Forms.ProgressBar();
            this.generateResultLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.averageTimeLabel = new System.Windows.Forms.Label();
            this.unsortedDataLabel = new System.Windows.Forms.Label();
            this.sortedDataLabel = new System.Windows.Forms.Label();
            this.timesLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // sortButton
            // 
            this.sortButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sortButton.Location = new System.Drawing.Point(3, 131);
            this.sortButton.Margin = new System.Windows.Forms.Padding(3, 15, 3, 3);
            this.sortButton.Name = "sortButton";
            this.sortButton.Size = new System.Drawing.Size(166, 30);
            this.sortButton.TabIndex = 11;
            this.sortButton.Text = "Sortér data";
            this.sortButton.UseVisualStyleBackColor = true;
            this.sortButton.Click += new System.EventHandler(this.sortButton_Click);
            // 
            // algorithmComboBox
            // 
            this.algorithmComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.algorithmComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.algorithmComboBox.FormattingEnabled = true;
            this.algorithmComboBox.Location = new System.Drawing.Point(3, 23);
            this.algorithmComboBox.Name = "algorithmComboBox";
            this.algorithmComboBox.Size = new System.Drawing.Size(166, 28);
            this.algorithmComboBox.TabIndex = 8;
            // 
            // dataSizeTextBox
            // 
            this.dataSizeTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataSizeTextBox.Location = new System.Drawing.Point(3, 87);
            this.dataSizeTextBox.Name = "dataSizeTextBox";
            this.dataSizeTextBox.Size = new System.Drawing.Size(166, 26);
            this.dataSizeTextBox.TabIndex = 4;
            this.dataSizeTextBox.Text = "10000";
            this.dataSizeTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dataSizeTextBox_KeyPress);
            // 
            // sortResultLabel
            // 
            this.sortResultLabel.AutoSize = true;
            this.sortResultLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sortResultLabel.Location = new System.Drawing.Point(3, 200);
            this.sortResultLabel.Name = "sortResultLabel";
            this.sortResultLabel.Size = new System.Drawing.Size(21, 20);
            this.sortResultLabel.TabIndex = 13;
            this.sortResultLabel.Text = "   ";
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.WorkerReportsProgress = true;
            this.backgroundWorker.WorkerSupportsCancellation = true;
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWork);
            this.backgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker_ProgressChanged);
            this.backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker_RunWorkerCompleted);
            // 
            // dataTypeLabel
            // 
            this.dataTypeLabel.AutoSize = true;
            this.dataTypeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataTypeLabel.Location = new System.Drawing.Point(3, 0);
            this.dataTypeLabel.Name = "dataTypeLabel";
            this.dataTypeLabel.Size = new System.Drawing.Size(78, 20);
            this.dataTypeLabel.TabIndex = 1;
            this.dataTypeLabel.Text = "Datatype:";
            // 
            // dataSizeLabel
            // 
            this.dataSizeLabel.AutoSize = true;
            this.dataSizeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataSizeLabel.Location = new System.Drawing.Point(3, 64);
            this.dataSizeLabel.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.dataSizeLabel.Name = "dataSizeLabel";
            this.dataSizeLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dataSizeLabel.Size = new System.Drawing.Size(125, 20);
            this.dataSizeLabel.TabIndex = 3;
            this.dataSizeLabel.Text = "Antal elementer:";
            // 
            // algorithmLabel
            // 
            this.algorithmLabel.AutoSize = true;
            this.algorithmLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.algorithmLabel.Location = new System.Drawing.Point(3, 0);
            this.algorithmLabel.Name = "algorithmLabel";
            this.algorithmLabel.Size = new System.Drawing.Size(151, 20);
            this.algorithmLabel.TabIndex = 7;
            this.algorithmLabel.Text = "Sorteringsalgoritme:";
            // 
            // dataTypeComboBox
            // 
            this.dataTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dataTypeComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataTypeComboBox.FormattingEnabled = true;
            this.dataTypeComboBox.Location = new System.Drawing.Point(3, 23);
            this.dataTypeComboBox.Name = "dataTypeComboBox";
            this.dataTypeComboBox.Size = new System.Drawing.Size(166, 28);
            this.dataTypeComboBox.TabIndex = 2;
            this.dataTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.dataTypeComboBox_SelectedIndexChanged);
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "Tekstfiler|*.txt|Alle filer|*.*";
            // 
            // generateButton
            // 
            this.generateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.generateButton.Location = new System.Drawing.Point(3, 131);
            this.generateButton.Margin = new System.Windows.Forms.Padding(3, 15, 3, 3);
            this.generateButton.Name = "generateButton";
            this.generateButton.Size = new System.Drawing.Size(166, 30);
            this.generateButton.TabIndex = 5;
            this.generateButton.Text = "Generér data";
            this.generateButton.UseVisualStyleBackColor = true;
            this.generateButton.Click += new System.EventHandler(this.generateButton_Click);
            // 
            // unsortedDataTextBox
            // 
            this.unsortedDataTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.unsortedDataTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.unsortedDataTextBox.Location = new System.Drawing.Point(3, 26);
            this.unsortedDataTextBox.Multiline = true;
            this.unsortedDataTextBox.Name = "unsortedDataTextBox";
            this.unsortedDataTextBox.ReadOnly = true;
            this.unsortedDataTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.unsortedDataTextBox.Size = new System.Drawing.Size(452, 113);
            this.unsortedDataTextBox.TabIndex = 15;
            // 
            // sortedDataTextBox
            // 
            this.sortedDataTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sortedDataTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sortedDataTextBox.Location = new System.Drawing.Point(3, 180);
            this.sortedDataTextBox.Multiline = true;
            this.sortedDataTextBox.Name = "sortedDataTextBox";
            this.sortedDataTextBox.ReadOnly = true;
            this.sortedDataTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sortedDataTextBox.Size = new System.Drawing.Size(452, 113);
            this.sortedDataTextBox.TabIndex = 17;
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "txt";
            this.saveFileDialog.Filter = "Tekstfiler|*.txt|Alle filer|*.*";
            // 
            // saveFileButton
            // 
            this.saveFileButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveFileButton.Location = new System.Drawing.Point(3, 167);
            this.saveFileButton.Name = "saveFileButton";
            this.saveFileButton.Size = new System.Drawing.Size(166, 30);
            this.saveFileButton.TabIndex = 6;
            this.saveFileButton.Text = "Gem sorterede data";
            this.saveFileButton.UseVisualStyleBackColor = true;
            this.saveFileButton.Click += new System.EventHandler(this.saveFileButton_Click);
            // 
            // repetitionsTextBox
            // 
            this.repetitionsTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repetitionsTextBox.Location = new System.Drawing.Point(3, 87);
            this.repetitionsTextBox.Name = "repetitionsTextBox";
            this.repetitionsTextBox.Size = new System.Drawing.Size(166, 26);
            this.repetitionsTextBox.TabIndex = 10;
            this.repetitionsTextBox.Text = "1";
            this.repetitionsTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.repetitionsTextBox_KeyPress);
            // 
            // repetitionsLabel
            // 
            this.repetitionsLabel.AutoSize = true;
            this.repetitionsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repetitionsLabel.Location = new System.Drawing.Point(3, 64);
            this.repetitionsLabel.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.repetitionsLabel.Name = "repetitionsLabel";
            this.repetitionsLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.repetitionsLabel.Size = new System.Drawing.Size(101, 20);
            this.repetitionsLabel.TabIndex = 9;
            this.repetitionsLabel.Text = "Gentagelser:";
            // 
            // timesTextBox
            // 
            this.timesTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.timesTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timesTextBox.Location = new System.Drawing.Point(3, 334);
            this.timesTextBox.Multiline = true;
            this.timesTextBox.Name = "timesTextBox";
            this.timesTextBox.ReadOnly = true;
            this.timesTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.timesTextBox.Size = new System.Drawing.Size(452, 113);
            this.timesTextBox.TabIndex = 19;
            // 
            // sortProgressBar
            // 
            this.sortProgressBar.Location = new System.Drawing.Point(3, 167);
            this.sortProgressBar.Name = "sortProgressBar";
            this.sortProgressBar.Size = new System.Drawing.Size(166, 30);
            this.sortProgressBar.TabIndex = 12;
            // 
            // generateResultLabel
            // 
            this.generateResultLabel.AutoSize = true;
            this.generateResultLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.generateResultLabel.Location = new System.Drawing.Point(175, 0);
            this.generateResultLabel.Name = "generateResultLabel";
            this.generateResultLabel.Size = new System.Drawing.Size(13, 40);
            this.generateResultLabel.TabIndex = 17;
            this.generateResultLabel.Text = "   ";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 2, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(784, 511);
            this.tableLayoutPanel1.TabIndex = 22;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel2, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel3, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(63, 18);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 220F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(194, 475);
            this.tableLayoutPanel2.TabIndex = 23;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.dataTypeLabel);
            this.flowLayoutPanel2.Controls.Add(this.dataTypeComboBox);
            this.flowLayoutPanel2.Controls.Add(this.dataSizeLabel);
            this.flowLayoutPanel2.Controls.Add(this.dataSizeTextBox);
            this.flowLayoutPanel2.Controls.Add(this.generateButton);
            this.flowLayoutPanel2.Controls.Add(this.saveFileButton);
            this.flowLayoutPanel2.Controls.Add(this.generateResultLabel);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(188, 214);
            this.flowLayoutPanel2.TabIndex = 25;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.algorithmLabel);
            this.flowLayoutPanel3.Controls.Add(this.algorithmComboBox);
            this.flowLayoutPanel3.Controls.Add(this.repetitionsLabel);
            this.flowLayoutPanel3.Controls.Add(this.repetitionsTextBox);
            this.flowLayoutPanel3.Controls.Add(this.sortButton);
            this.flowLayoutPanel3.Controls.Add(this.sortProgressBar);
            this.flowLayoutPanel3.Controls.Add(this.sortResultLabel);
            this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(3, 223);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(188, 244);
            this.flowLayoutPanel3.TabIndex = 26;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.timesTextBox, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.unsortedDataTextBox, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.sortedDataTextBox, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.averageTimeLabel, 0, 6);
            this.tableLayoutPanel3.Controls.Add(this.unsortedDataLabel, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.sortedDataLabel, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.timesLabel, 0, 4);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(263, 18);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 7;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(458, 475);
            this.tableLayoutPanel3.TabIndex = 24;
            // 
            // averageTimeLabel
            // 
            this.averageTimeLabel.AutoSize = true;
            this.averageTimeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.averageTimeLabel.Location = new System.Drawing.Point(3, 450);
            this.averageTimeLabel.Name = "averageTimeLabel";
            this.averageTimeLabel.Size = new System.Drawing.Size(21, 20);
            this.averageTimeLabel.TabIndex = 16;
            this.averageTimeLabel.Text = "   ";
            // 
            // unsortedDataLabel
            // 
            this.unsortedDataLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.unsortedDataLabel.AutoSize = true;
            this.unsortedDataLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.unsortedDataLabel.Location = new System.Drawing.Point(3, 3);
            this.unsortedDataLabel.Name = "unsortedDataLabel";
            this.unsortedDataLabel.Size = new System.Drawing.Size(111, 20);
            this.unsortedDataLabel.TabIndex = 14;
            this.unsortedDataLabel.Text = "Originale data:";
            // 
            // sortedDataLabel
            // 
            this.sortedDataLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.sortedDataLabel.AutoSize = true;
            this.sortedDataLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sortedDataLabel.Location = new System.Drawing.Point(3, 157);
            this.sortedDataLabel.Name = "sortedDataLabel";
            this.sortedDataLabel.Size = new System.Drawing.Size(120, 20);
            this.sortedDataLabel.TabIndex = 16;
            this.sortedDataLabel.Text = "Sorterede data:";
            // 
            // timesLabel
            // 
            this.timesLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.timesLabel.AutoSize = true;
            this.timesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timesLabel.Location = new System.Drawing.Point(3, 311);
            this.timesLabel.Name = "timesLabel";
            this.timesLabel.Size = new System.Drawing.Size(374, 20);
            this.timesLabel.TabIndex = 18;
            this.timesLabel.Text = "Forløbne tidsrum i millisekunder for hver gentagelse:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 511);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(750, 550);
            this.Name = "Form1";
            this.Text = "SOP: Sammenligning af sorteringsalgoritmer - David JT";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel3.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button sortButton;
        private System.Windows.Forms.ComboBox algorithmComboBox;
        private System.Windows.Forms.TextBox dataSizeTextBox;
        private System.Windows.Forms.Label sortResultLabel;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private System.Windows.Forms.Label dataTypeLabel;
        private System.Windows.Forms.Label dataSizeLabel;
        private System.Windows.Forms.Label algorithmLabel;
        private System.Windows.Forms.ComboBox dataTypeComboBox;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button generateButton;
        private System.Windows.Forms.TextBox unsortedDataTextBox;
        private System.Windows.Forms.TextBox sortedDataTextBox;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.Button saveFileButton;
        private System.Windows.Forms.TextBox repetitionsTextBox;
        private System.Windows.Forms.Label repetitionsLabel;
        private System.Windows.Forms.TextBox timesTextBox;
        private System.Windows.Forms.ProgressBar sortProgressBar;
        private System.Windows.Forms.Label generateResultLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label averageTimeLabel;
        private System.Windows.Forms.Label unsortedDataLabel;
        private System.Windows.Forms.Label sortedDataLabel;
        private System.Windows.Forms.Label timesLabel;
    }
}

