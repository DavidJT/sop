﻿/*
 * David JT; December 18, 2020.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOP
{
    public abstract class Data<T> where T : IComparable<T>
    {
        protected T[] _data;

        // Allow indexing this object to access the data array.
        public T this[int index]
        {
            get { return _data[index]; }
            set { _data[index] = value; }
        }

        public int Length
        {
            get { return _data.Length; }
        }

        /// <summary>
        /// Creates a new empty Data object.
        /// </summary>
        public Data() { }

        /// <summary>
        /// Creates a new Data object around an existing data array.
        /// </summary>
        /// <param name="data">The existing data array.</param>
        public Data(T[] data)
        {
            _data = data;
        }

        /// <summary>
        /// Creates a new Data object as a copy of a given Data object.
        /// </summary>
        /// <param name="data">The existing Data object to copy.</param>
        public Data(Data<T> data)
        {
            _data = new T[data.Length];
            Array.Copy(data.GetAll(), _data, data.Length);
        }

        /// <summary>
        /// Returns a copy of this Data object.
        /// </summary>
        /// <returns>A copy of this Data object.</returns>
        public abstract Data<T> Copy();

        /// <summary>
        /// Returns the complete data array in this Data object.
        /// </summary>
        /// <returns>The data array in this Data object.</returns>
        public T[] GetAll()
        {
            return _data;
        }
    }
}
