﻿/*
 * David JT; December 18, 2020.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOP
{
    public class MergeSort : SortingAlgorithm
    {
        /// <summary>
        /// Sorts the given Data object in-place using merge sort.
        /// </summary>
        /// <typeparam name="T">The type of the Data object.</typeparam>
        /// <param name="data">The Data object to sort in-place.</param>
        public override void Sort<T>(Data<T> data)
        {
            Sort(data, data.Copy(), 0, data.Length);
        }

        /// <summary>
        /// Sorts the given Data object in-place using merge sort between a given start and end
        /// index.
        /// </summary>
        /// <typeparam name="T">The type of the Data object.</typeparam>
        /// <param name="data">The Data object to sort in-place.</param>
        /// <param name="copy">A copy of the Data object that is being sorted.</param>
        /// <param name="start">The inclusive index to sort from.</param>
        /// <param name="end">The exlusive index to sort to.</param>
        private void Sort<T>(Data<T> data, Data<T> copy, int start, int end) where T : IComparable<T>
        {
            int partLength = end - start;

            // Single element subparts are per definition already sorted.
            if (partLength < 2)
            {
                return;
            }

            // Compare two elements directly instead of merging one-element subparts.
            if (partLength == 2)
            {
                int next = start + 1;
                if (data[start].CompareTo(data[next]) > 0)
                {
                    // Swap the elements that are out of order with tuple syntax.
                    (data[start], data[next]) = (data[next], data[start]);
                }

                return;
            }

            int mid = (start + end) / 2;

            // Recursively sort the left and right subparts.
            Sort(copy, data, start, mid);
            Sort(copy, data, mid, end);

            // Merge the left and right subparts by continuously selecting the smallest element
            // from either the left or right part (or from the other part if one is exhausted).
            int leftIndex = start;
            int rightIndex = mid;
            for (int insertIndex = start; insertIndex < end; insertIndex++)
            {
                if (rightIndex >= end
                    || (leftIndex < mid && copy[leftIndex].CompareTo(copy[rightIndex]) <= 0))
                {
                    data[insertIndex] = copy[leftIndex];
                    leftIndex++;
                }
                else
                {
                    data[insertIndex] = copy[rightIndex];
                    rightIndex++;
                }
            }
        }
    }
}
