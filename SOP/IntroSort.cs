﻿/*
 * David JT; December 18, 2020.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOP
{
    public class IntroSort : SortingAlgorithm
    {
        /// <summary>
        /// Sorts the given Data object in-place using introsort.
        /// </summary>
        /// <typeparam name="T">The type of the Data object.</typeparam>
        /// <param name="data">The Data object to sort in-place.</param>
        public override void Sort<T>(Data<T> data)
        {
            int depthLimit = (int)Math.Log(data.Length, 2);
            Sort(data, 0, data.Length - 1, depthLimit);
        }

        /// <summary>
        /// Sorts the given Data object in-place using introsort between a left and right index.
        /// </summary>
        /// <typeparam name="T">The type of the Data object.</typeparam>
        /// <param name="data">The Data object to sort in-place.</param>
        /// <param name="left">The inclusive left index to sort from.</param>
        /// <param name="right">The inclusive right index to sort to.</param>
        private void Sort<T>(Data<T> data, int left, int right, int depthLimit) where T : IComparable<T>
        {
            // Sort parts of the data with 16 elements or less using insertion sort.
            int length = right - left + 1;
            if (length > 1 && length <= 16)
            {
                Algorithms.InsertionSort.Sort(data, left, right);
                return;
            }

            // If the recursive partitioning has reached the maximum depth, use heap sort.
            if (depthLimit <= 0)
            {
                Algorithms.HeapSort.Sort(data, left, right);
                return;
            }

            // Otherwise, use quicksort.
            int pivot = Algorithms.QuickSort.Partition(data, left, right);

            // Sort subparts of the data that are at least of length 2.
            if (pivot - left >= 2)
            {
                Sort(data, left, pivot - 1, depthLimit - 1);
            }
            if (right - pivot >= 2)
            {
                Sort(data, pivot + 1, right, depthLimit - 1);
            }
        }
    }
}
