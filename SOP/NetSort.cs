﻿/*
 * David JT; December 18, 2020.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOP
{
    public class NetSort : SortingAlgorithm
    {
        /// <summary>
        /// Sorts the given Data object in-place using the Array.Sort method in the .NET API.
        /// </summary>
        /// <typeparam name="T">The type of the Data object.</typeparam>
        /// <param name="data">The Data object to sort in-place.</param>
        public override void Sort<T>(Data<T> data)
        {
            Array.Sort(data.GetAll());
        }
    }
}
