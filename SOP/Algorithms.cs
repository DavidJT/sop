﻿/*
 * David JT; December 18, 2020.
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOP
{
    /// <summary>
    /// Class for holding static instances of each sorting algorithm since only one instance of
    /// each is needed.
    /// </summary>
    public static class Algorithms
    {
        public static InsertionSort InsertionSort = new InsertionSort();
        public static MergeSort MergeSort = new MergeSort();
        public static HeapSort HeapSort = new HeapSort();
        public static QuickSort QuickSort = new QuickSort();
        public static IntroSort IntroSort = new IntroSort();
        public static NetSort NetSort = new NetSort();
    }
}
