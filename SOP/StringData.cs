﻿/*
 * David JT; December 18, 2020.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOP
{
    public class StringData : Data<string>
    {
        public StringData() { }

        public StringData(string[] data) : base(data) { }

        public StringData(StringData data) : base(data) { }

        public override Data<string> Copy()
        {
            return new StringData(this);
        }
    }
}
