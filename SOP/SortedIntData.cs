﻿/*
 * David JT; December 18, 2020.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOP
{
    public class SortedIntData : IntData
    {
        public SortedIntData(int[] data) : base(data) { }

        public SortedIntData(SortedIntData data) : base(data) { }

        /// <summary>
        /// Creates a new Data object of a specified amount of sorted integers that are evenly
        /// distributed within the maximum range for a 32-bit integer.
        /// </summary>
        /// <param name="amount">The amount of integers to generate.</param>
        public SortedIntData(int amount) : this(amount, Int32.MinValue, Int32.MaxValue) { }

        /// <summary>
        /// Creates a new Data object of a specified amount of sorted integers that are evenly
        /// distributed within a given range.
        /// </summary>
        /// <param name="amount">The amount of integers to generate.</param>
        /// <param name="min">The inclusive minimum value for the generated integers.</param>
        /// <param name="max">The inclusive maximum value for the generated integers.</param>
        public SortedIntData(int amount, int min, int max)
        {
            _data = new int[amount];

            // Calculate the distance between each integer to evenly distribute them.
            double distance = ((double)max - min) / (amount - 1);
            for (int i = 0; i < amount; i++)
            {
                _data[i] = (int)(min + i * distance);
            }
        }

        public override Data<int> Copy()
        {
            return new SortedIntData(this);
        }
    }
}
