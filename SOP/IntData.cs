﻿/*
 * David JT; December 18, 2020.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOP
{
    public class IntData : Data<int>
    {
        public IntData() { }

        public IntData(int[] data) : base(data) { }

        public IntData(IntData data) : base(data) { }

        public override Data<int> Copy()
        {
            return new IntData(this);
        }
    }
}
