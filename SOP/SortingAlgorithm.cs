﻿/*
 * David JT; December 18, 2020.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOP
{
    public abstract class SortingAlgorithm
    {
        /// <summary>
        /// Sorts the given Data object in-place using this SortingAlgorithm.
        /// </summary>
        /// <typeparam name="T">The type of the Data object.</typeparam>
        /// <param name="data">The Data object to sort in-place.</param>
        public abstract void Sort<T>(Data<T> data) where T : IComparable<T>;

        /// <summary>
        /// Sorts the given Data object in-place using this SortingAlgorithm and returns the time
        /// that the actual sorting took.
        /// </summary>
        /// <typeparam name="T">The type of the Data object.</typeparam>
        /// <param name="data">The Data object to sort in-place.</param>
        /// <returns>The time that it took to sort the Data object.</returns>
        public long TimedSort<T>(Data<T> data) where T : IComparable<T>
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();

            Sort(data);

            watch.Stop();

            // Instead of returning the exact amount of timer ticks that have passed
            // (watch.ElapsedTicks), we just use milliseconds as the time unit. Uncertainties
            // caused by other factors seemed to have too much of an impact on the exact numbers
            // otherwise; e.g. something like a small slowdown of the CPU (perhaps because of other
            // running programs) meant that repeated measurements often varied by 20% or more.
            // Therefore, it doesn't feel necessary to use the highest amount of precision. It
            // seems better to just force the user to use the program for larger data sets (so that
            // the reported time isn't always 0 milliseconds).
            return watch.ElapsedMilliseconds;
        }
    }
}
